#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `workaholic` package."""

from datetime import date, datetime

import pytest
import pytz
from click.testing import CliRunner
from workalendar.europe import Germany

from workaholic import cli, workaholic

paris = pytz.timezone("Europe/Paris")


@pytest.mark.parametrize(
    "dt, tz, expected",
    [
        # spring in France
        (
            datetime(2019, 6, 3, 21, 59, tzinfo=pytz.UTC),
            None,
            datetime(2019, 6, 3, 23, 59),
        ),
        # spring in France
        (
            datetime(2019, 6, 3, 22, 0, tzinfo=pytz.UTC),
            None,
            datetime(2019, 6, 4, 0, 0),
        ),
        # winter in France
        (
            datetime(2019, 2, 3, 22, 59, tzinfo=pytz.UTC),
            None,
            datetime(2019, 2, 3, 23, 59),
        ),
        # winter in France
        (
            datetime(2019, 2, 3, 23, 0, tzinfo=pytz.UTC),
            None,
            datetime(2019, 2, 4, 0, 0),
        ),
        # tz UTC
        (
            datetime(2019, 2, 3, 23, 0, tzinfo=pytz.UTC),
            pytz.utc,
            datetime(2019, 2, 3, 23, 0),
        ),
    ],
)
def test_datetime_from_timestamp(dt, tz, expected):
    timestamp = datetime.timestamp(dt)
    dt = workaholic.datetime_from_timestamp(timestamp, tz=tz)
    expected_with_tz = (tz or paris).localize(expected)
    assert (
        workaholic.datetime_from_timestamp(timestamp, tz=tz) == expected_with_tz
    )


@pytest.mark.parametrize(
    "commit_date, cal, expected",
    [
        (date(2012, 12, 25), None, False),  # xmas
        (date(2019, 6, 4), None, True),  # simple tuesday
        (date(2019, 5, 30), None, False),  # off in France
        (date(2019, 6, 2), None, False),  # sunday
        (date(2019, 5, 8), None, False),  # off in France
        (date(2019, 5, 8), Germany(), True),  # but not in Germany
    ],
)
def test_is_working_day(commit_date, cal, expected):
    assert workaholic.is_working_day(commit_date, cal=cal) == expected


@pytest.mark.parametrize(
    "commit_datetime, expected",
    [
        (datetime(2019, 6, 3, 7, 59, tzinfo=paris), False),
        (datetime(2019, 6, 3, 8, 0, tzinfo=paris), True),
        (datetime(2019, 6, 3, 12, 42, tzinfo=paris), True),
        (datetime(2019, 6, 3, 7, 59, tzinfo=pytz.UTC), False),
        (datetime(2019, 6, 3, 8, 0, tzinfo=pytz.UTC), True),
        (datetime(2019, 6, 3, 12, 42, tzinfo=pytz.UTC), True),
        (datetime(2019, 6, 3, 20, 0, tzinfo=paris), True),
        (datetime(2019, 6, 3, 20, 1, tzinfo=paris), False),
        (datetime(2019, 6, 3, 23, 35, tzinfo=paris), False),
        (datetime(2019, 6, 3, 20, 0, tzinfo=pytz.UTC), True),
        (datetime(2019, 6, 3, 20, 1, tzinfo=pytz.UTC), False),
        (datetime(2019, 6, 3, 23, 35, tzinfo=pytz.UTC), False),
    ],
)
def test_is_in_working_hours(commit_datetime, expected):
    assert workaholic.is_in_working_hours(commit_datetime) == expected


@pytest.mark.parametrize(
    "commit_datetime, expected",
    [
        (datetime(2019, 6, 3, 12, 42, tzinfo=paris), True),
        (datetime(2019, 6, 3, 12, 42, tzinfo=pytz.UTC), True),
        (datetime(2019, 6, 3, 23, 35, tzinfo=paris), False),
        (datetime(2019, 6, 3, 23, 35, tzinfo=pytz.UTC), False),
        (datetime(2019, 6, 9, 12, 42, tzinfo=paris), False),
        (datetime(2019, 6, 9, 12, 42, tzinfo=pytz.UTC), False),
        (datetime(2019, 6, 9, 23, 35, tzinfo=paris), False),
        (datetime(2019, 6, 9, 23, 35, tzinfo=pytz.UTC), False),
    ],
)
def test_is_correct(commit_datetime, expected):
    assert workaholic.is_correct(commit_datetime) == expected


@pytest.mark.parametrize(
    "good, bad, expected", [(0, 0, 0), (42, 0, 0), (0, 42, 100), (10, 30, 75)]
)
def test_bad_commit_rate(good, bad, expected):
    stat = workaholic.CommitStat(good=good, bad=bad)
    assert workaholic.bad_commit_rate(stat) == expected


@pytest.fixture()
def commits(mocker):
    author_a = mocker.MagicMock(email="a@gmail.com")
    author_b = mocker.MagicMock(email="b@gmail.com")
    author_c = mocker.MagicMock(email="c@gmail.com")
    return [
        mocker.MagicMock(
            author=author_a,
            authored_date=datetime.timestamp(
                paris.localize(datetime(2019, 6, 3, 7, 59))
            ),
        ),
        mocker.MagicMock(
            author=author_b,
            authored_date=datetime.timestamp(
                paris.localize(datetime(2019, 6, 3, 12, 42))
            ),
        ),
        mocker.MagicMock(
            author=author_c,
            authored_date=datetime.timestamp(
                paris.localize(datetime(2019, 5, 3, 7, 59))
            ),
        ),
        mocker.MagicMock(
            author=author_c,
            authored_date=datetime.timestamp(
                paris.localize(datetime(2019, 5, 3, 12, 42))
            ),
        ),
    ]


def test_analyze_commits(mocker, commits):
    assert workaholic.analyze_commits(commits) == {
        "a@gmail.com": workaholic.CommitStat(bad=1, good=0),
        "b@gmail.com": workaholic.CommitStat(bad=0, good=1),
        "c@gmail.com": workaholic.CommitStat(bad=1, good=1),
    }


def test_build_report(mocker, commits):
    get_commits = mocker.patch(
        "workaholic.workaholic.get_commits", return_value=commits
    )
    analyze_commits = mocker.patch(
        "workaholic.workaholic.analyze_commits",
        return_value={
            "a@gmail.com": workaholic.CommitStat(bad=1, good=0),
            "b@gmail.com": workaholic.CommitStat(bad=0, good=1),
            "c@gmail.com": workaholic.CommitStat(bad=1, good=1),
        },
    )
    assert workaholic.build_report(
        "/path/to/some/repo", branch="foo-bar-branch"
    ) == (
        "a@gmail.com: 100.00% (1 bad commits/0 good commits)\n"
        "c@gmail.com: 50.00% (1 bad commits/1 good commits)\n"
        "b@gmail.com: 0.00% (0 bad commits/1 good commits)"
    )
    assert get_commits.call_args_list == [
        mocker.call("/path/to/some/repo", "foo-bar-branch")
    ]
    assert analyze_commits.call_args_list == [mocker.call(commits)]


def test_command_line_interface(mocker):
    mocker.patch("workaholic.workaholic.build_report", return_value="Foo Bar !")
    runner = CliRunner()
    result = runner.invoke(cli.cli, ["/foo/bar"])
    assert result.exit_code == 0
    assert "Foo Bar !" in result.output
