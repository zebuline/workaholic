# -*- coding: utf-8 -*-

"""Console script for workaholic."""

import click

from workaholic import workaholic


@click.command()
@click.argument("repo_path")
@click.option("--branch", "-b", default="master", help="Branch to inspect")
def cli(repo_path, branch):
    click.echo(workaholic.build_report(repo_path, branch))
    return 0
