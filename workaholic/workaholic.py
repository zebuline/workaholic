# -*- coding: utf-8 -*-

"""Main module."""

from collections import defaultdict
from datetime import datetime

import attr
import pytz
from git import GitCommandError, Repo
from workalendar.europe import France


@attr.dataclass
class CommitStat:
    # changes committed during a working day, between 8am and 8pm
    good: int = 0
    # the other changes
    bad: int = 0


def datetime_from_timestamp(timestamp, tz=None):
    tz = tz or pytz.timezone("Europe/Paris")
    return datetime.fromtimestamp(timestamp, tz=tz)


def is_working_day(commit_date, cal=None):
    cal = cal or France()
    return cal.is_working_day(commit_date)


def is_in_working_hours(commit_datetime):
    # commit_datetime is already in the correct timezone
    dt_8 = commit_datetime.replace(hour=8, minute=0, second=0, microsecond=0)
    dt_20 = commit_datetime.replace(hour=20, minute=0, second=0, microsecond=0)
    return dt_8 <= commit_datetime <= dt_20


def is_correct(commit_datetime):
    """
    Return True if the given datetime is in a working day,
    during working hours.
    Note: base on the french calendar, and Europe/Paris timezone
    """
    if not is_working_day(commit_datetime.date()):
        return False
    return is_in_working_hours(commit_datetime)


def bad_commit_rate(stat):
    if not stat.good and not stat.bad:
        return 0
    return (stat.bad / (stat.good + stat.bad)) * 100


def get_commits(repo_path, branch):
    """
    Return all commits of the given branch
    """
    repo = Repo(repo_path)
    return repo.iter_commits(branch)


def analyze_commits(commits):
    """
    Return a dict of CommitStat:
    {
        'author_email': CommitStat()
    }

    Note: base on the french calendar, and Europe/Paris timezone
    """
    author_counters = defaultdict(CommitStat)

    for commit in commits:
        # do we have to check the committer ?
        # I chose to check the author only
        commit_dt = datetime_from_timestamp(commit.authored_date)
        # XXX find a way to deduplicate authors ?
        author = commit.author.email

        if is_correct(commit_dt):
            author_counters[author].good += 1
        else:
            author_counters[author].bad += 1

    return author_counters


def build_report(repo_path, branch="master"):
    # get commits and analyze them
    try:
        commits = get_commits(repo_path, branch)
        analysis = analyze_commits(commits)
    except GitCommandError as exception:
        return "Can not read commits: {}".format(exception)

    # calculate the rate of bad commits by author
    bad_commit_rates = {
        author: bad_commit_rate(stat) for author, stat in analysis.items()
    }

    # sort authors by bad results
    authors = sorted(
        bad_commit_rates, key=lambda k: bad_commit_rates[k], reverse=True
    )

    # print the result
    result = []
    for author in authors:
        result.append(
            "{}: {:.2f}% ({} bad commits/{} good commits)".format(
                author,
                bad_commit_rates[author],
                analysis[author].bad,
                analysis[author].good,
            )
        )
    return "\n".join(result)
