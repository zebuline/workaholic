# -*- coding: utf-8 -*-

"""Top-level package for workaholic."""

__author__ = """Lauréline Guérin"""
__email__ = "zebuline@crocobox.org"
__version__ = "0.1.0"
