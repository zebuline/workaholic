==========
workaholic
==========


simple script to track workaholic devs


* Free software: MIT license
* Documentation: https://workaholic.readthedocs.io.


Get Started!
------------

1. Clone the repo::

    $ git clone git@framagit.org:zebuline/workaholic.git

2. Install your local copy into a virtualenv. Assuming you have virtualenvwrapper installed, this is how you set up your fork for local development::

    $ mkvirtualenv workaholic -p python3
    $ cd workaholic/
    $ make develop

3. Be sure tests are running::

    $ tox

   To get tox, just pip install them into your virtualenv.

How it works
------------

To inspect a git repository (cloned locally), just do::

    $ workaholic /path/to/a/git/repo

To inspect a specific branch::

    $ workaholic /path/to/a/git/repo -b <branch_name>

Or::

    $ workaholic /path/to/a/git/repo --branch <branch_name>

Example of result (emails were anonymized)::

    <anonymized_email>: 50.00% (2 bad commits/2 good commits)
    <anonymized_email>: 31.01% (147 bad commits/327 good commits)
    <anonymized_email>: 21.61% (110 bad commits/399 good commits)
    <anonymized_email>: 19.38% (44 bad commits/183 good commits)
    <anonymized_email>: 14.29% (4 bad commits/24 good commits)
    <anonymized_email>: 11.00% (22 bad commits/178 good commits)
    <anonymized_email>: 6.03% (7 bad commits/109 good commits)
    <anonymized_email>: 3.17% (2 bad commits/61 good commits)
    <anonymized_email>: 0.00% (0 bad commits/3 good commits)
    <anonymized_email>: 0.00% (0 bad commits/5 good commits)
    <anonymized_email>: 0.00% (0 bad commits/2 good commits)
    <anonymized_email>: 0.00% (0 bad commits/1 good commits)
    <anonymized_email>: 0.00% (0 bad commits/1 good commits)
    <anonymized_email>: 0.00% (0 bad commits/10 good commits)

The rate describes the part of commits made outside working hours.

Works only for France for now (based on Paris timezone and France calendar -
see https://peopledoc.github.io/workalendar/), but it could be parametrized.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
